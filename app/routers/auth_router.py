from app import api
from flask_restx import Resource
from flask import request
from flask_jwt_extended import jwt_required, get_jwt_identity
from app.schemas.auth_schema import AuthRequestSchema
from app.controllers.auth_controller import AuthController

auth_ns = api.namespace(
    name='Autenticación',
    description='Endpoints del modulo Auth',
    path='/auth'
)

schema_request = AuthRequestSchema(auth_ns)


@auth_ns.route('/signin')
class AuthSignIn(Resource):
    @auth_ns.expect(schema_request.login(), validate=True)
    def post(self):
        ''' Login de usuario '''
        controller = AuthController()
        return controller.signIn(request.json)


@auth_ns.route('/token/refresh')
class AuthTokenRefresh(Resource):
    @jwt_required(refresh=True)
    @auth_ns.expect(schema_request.refresh())
    def post(self):
        ''' Obtener un nuevo access_token '''
        controller = AuthController()
        return controller.refreshToken(get_jwt_identity())


@auth_ns.route('/password/reset')
class AuthResetPassword(Resource):
    @auth_ns.expect(schema_request.forgot_password(), validate=True)
    def post(self):
        ''' Reiniciar contraseña '''
        controller = AuthController()
        return controller.reset_password(request.json)

@auth_ns.route('/password/change')
@auth_ns.doc(security='Bearer')
class AuthChangePassword(Resource):
    @jwt_required()
    @auth_ns.expect(schema_request.change_password(), validate=True)
    def put(self):
        controller = AuthController()
        return controller.change_password(request.json)
