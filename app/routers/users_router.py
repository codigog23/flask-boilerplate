from app import api
from flask import request
from flask_restx import Resource
from flask_jwt_extended import jwt_required
from app.controllers.users_controller import UserController
from app.schemas.users_schema import UserRequestSchema

# http://api.dominio.com/users

user_ns = api.namespace(
    name='Usuarios',
    description='Endpoints del modulo Usuario',
    path='/users'
)

schema_request = UserRequestSchema(user_ns)

# CRUD
@user_ns.route('')
@user_ns.doc(security='Bearer')
class UserRouter(Resource):
    @jwt_required()
    @user_ns.expect(schema_request.all())
    def get(self):
        ''' Listar los usuarios '''
        query = schema_request.all().parse_args()
        controller = UserController()
        return controller.fetch_all(query)

    @jwt_required()
    @user_ns.expect(schema_request.create(), validate=True)
    def post(self):
        ''' Crear un usuario '''
        controller = UserController()
        return controller.save(request.json)

@user_ns.route('/<int:id>')
@user_ns.doc(security='Bearer')
class UserByIdRouter(Resource):
    @jwt_required()
    def get(self, id):
        ''' Obtener usuario por ID '''
        controller = UserController()
        return controller.find_by_id(id)

    @jwt_required()
    @user_ns.expect(schema_request.update(), validate=True)
    def put(self, id):
        ''' Actualizar usuario por ID '''
        controller = UserController()
        return controller.update(id, request.json)

    @jwt_required()
    def delete(self, id):
        ''' Eliminar usuario por ID '''
        controller = UserController()
        return controller.remove(id)
