from app import api
from flask import request
from flask_restx import Resource
from flask_jwt_extended import jwt_required
from app.controllers.roles_controller import RoleController
from app.schemas.roles_schema import RoleRequestSchema
from app.middlewares.role_required import role_required


role_ns = api.namespace(
    name='Roles',
    description='Endpoints del modulo Roles',
    path='/roles'
)


schema_request = RoleRequestSchema(role_ns)


@role_ns.route('')
@role_ns.doc(security='Bearer')
class RoleRouter(Resource):
    @jwt_required()
    @role_required(1)
    @role_ns.expect(schema_request.all())
    def get(self):
        ''' Listar los roles '''
        query = schema_request.all().parse_args()
        controller = RoleController()
        return controller.fetch_all(query)
    
    @jwt_required()
    @role_ns.expect(schema_request.create(), validate=True)
    def post(self):
        ''' Crear un rol '''
        controller = RoleController()
        return controller.save(request.json)

@role_ns.route('/<int:id>')
@role_ns.doc(security='Bearer')
class RoleByIdRouter(Resource):
    @jwt_required()
    def get(self, id):
        ''' Obtener rol por ID '''
        controller = RoleController()
        return controller.find_by_id(id)
    
    @jwt_required()
    @role_ns.expect(schema_request.update(), validate=True)
    def put(self, id):
        ''' Actualizar rol por ID '''
        controller = RoleController()
        return controller.update(id, request.json)
    
    @jwt_required()
    def delete(self, id):
        ''' Eliminar rol por ID '''
        controller = RoleController()
        return controller.remove(id)
