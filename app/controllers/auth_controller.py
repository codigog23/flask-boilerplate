from app import db
from os import getenv
from app.models.users_model import UserModel
from flask_jwt_extended import create_access_token, create_refresh_token, current_user
from http import HTTPStatus
from secrets import token_hex
from app.helpers.mailing import Mailing


class AuthController:
    def __init__(self):
        self.db = db
        self.model = UserModel
        self.mailing = Mailing()

    def signIn(self, body):
        try:
            username = body['username']
            password = body['password']

            # 1. Validar que el usuario exista y no este inactivo
            record = self.model.where(username=username, status=True).first()
            # 2. Validar que la contraseña sea correcta
            if record and record.check_password(password):
            # 3. Creación de los tokens
                    user_id = record.id
                    return {
                        'access_token': create_access_token(identity=user_id),
                        'refresh_token': create_refresh_token(identity=user_id)
                    }, HTTPStatus.OK
            return {
                'message': 'El usuario y/o las credenciales son incorrectas'
            }, HTTPStatus.UNAUTHORIZED
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def refreshToken(self, identity):
        try:
            return {
                'access_token': create_access_token(identity=identity)
            }, HTTPStatus.OK
        except Exception as e:
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
        
    def reset_password(self, body):
        try:
            email = body['email']

            # 1. Validar que el correo exista
            record = self.model.where(email=email, status=True).first()

            if not record:
                return {
                    'message': f'No se encontro el usuario con el correo {email}'
                }, HTTPStatus.NOT_FOUND
            
            # 2. Generar la nueva contraseña y actualizarla
            new_password = token_hex(5)
            record.password = new_password
            record.hash_password()

            self.db.session.add(record)
            self.db.session.commit()

            # 3. Notificar al usuario
            self.mailing.mail_reset_password(
                record.email, record.name, new_password
            )

            return {
                'message': 'Se reinicio tu contraseña con exito'
            }, HTTPStatus.OK
        except Exception as e:
            self.db.session.rollback()
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR

    def change_password(self, body):
        try:
            record = current_user
            record.password = body['new_password']
            record.hash_password()

            self.db.session.add(record)
            self.db.session.commit()

            return {
                'message': 'La contraseña se cambio con exito'
            }, HTTPStatus.OK
        except Exception as e:
            self.db.session.rollback()
            return {
                'message': 'Ocurrio un error',
                'error': e
            }, HTTPStatus.INTERNAL_SERVER_ERROR
