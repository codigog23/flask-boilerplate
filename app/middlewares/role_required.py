from functools import wraps
from flask_jwt_extended import current_user
from http import HTTPStatus

def role_required(filter_role_id): # nombre del decorador
    def wrapper(fn): # decorador real
        @wraps(fn)
        def decorator(*args, **kwargs):
            role_user = current_user.role_id
            if role_user == filter_role_id:
                return fn(*args, **kwargs)
            return {
                'message': 'No cuentas con el rol necesario'
            }, HTTPStatus.FORBIDDEN
        return decorator
    return wrapper
