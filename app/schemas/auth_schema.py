from flask_restx import fields
from flask_restx.reqparse import RequestParser


class AuthRequestSchema:
    def __init__(self, namespace):
        self.ns = namespace

    def login(self):
        return self.ns.model('Auth SignIn', {
            'username': fields.String(required=True),
            'password': fields.String(required=True)
        })
    
    def refresh(self):
        parser = RequestParser()
        parser.add_argument(
            'Authorization', type=str, location='headers', 
            help='Ex: Bearer {token}'
        )
        return parser
    
    def forgot_password(self):
        return self.ns.model('Auth Password Reset', {
            'email': fields.String(required=True)
        })
    
    def change_password(self):
        return self.ns.model('Auth Password Change', {
            'new_password': fields.String(required=True)
        })
