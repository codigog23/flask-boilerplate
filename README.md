![logo](https://www.python.org/static/community_logos/python-logo-generic.svg)

# Flask Boilerplate

---

## Modelos

- Usuarios (users)

| columna   | tipo         | constraint           |
| --------- | ------------ | -------------------- |
| id        | SERIAL       | PRIMARY KEY          |
| name      | VARCHAR(120) | NOT NULL             |
| last_name | VARCHAR(150) | NOT NULL             |
| age       | INTEGER      | NOT NULL             |
| username  | VARCHAR(80)  | UNIQUE               |
| password  | VARCHAR(255) | NOT NULL             |
| email     | VARCHAR(160) | UNIQUE               |
| role_id   | INTEGER      | FOREIGN KEY NOT NULL |
| status    | BOOLEAN      | DEFAULT TRUE         |

- Roles (roles)

| columna | tipo    | constraint   |
| ------- | ------- | ------------ |
| id      | SERIAL  | PRIMARY KEY  |
| name    | CHAR(8) | NOT NULL     |
| status  | BOOLEAN | DEFAULT TRUE |

## Caracteristicas

1. Autenticación
   - [x] Crear endpoints para registrar, logearse.
   - [x] Encriptación contraseña (bcrypt).
   - [x] Manejar token de autenticación (JWT).
2. Resetear Contraseña
   - [x] Generar una nueva contraseña encriptada.
   - [x] Enviar un correo con la nueva contraseña, usando un template.
3. CRUD para cada modulo
   - [x] Listado con paginación y filtros.
   - [x] Crear, Obtener, Actualizar y Eliminar (SOFTDELETE).
4. Decoradores
   - [x] Protección de rutas mediante el JWT.
   - [x] Protección de rutas mediante Roles.
5. Despliegue
   - [ ] Render
6. **Extra**:
   - [ ] Agregar campo de avatar al usuario.
   - [ ] Integrar el Bucket S3, para guardar la imagen.

## Environment (.env)

```py
FLASK_APP='main.py'
FLASK_RUN_HOST=127.0.0.1
FLASK_RUN_PORT=5000
FLASK_DEBUG=True
FLASK_ENV='development'

DATABASE_URL='postgresql://postgres:mysql@localhost:5432/flask_boilerplate'
SECRET_KEY='tecsup'

MAIL_SERVER='smtp.gmail.com'
MAIL_PORT=587
MAIL_USE_TLS=True
MAIL_USERNAME=''
MAIL_PASSWORD=''
```

## Servicios Cloud Computing

1. IaaS (Infraestuctura como servicio)
2. PaaS (Plataforma como servicio)
3. SaaS (Software como servicio)
